#coding=utf-8
from functools import wraps

'''
单例模式 Python3 实现
'''


def singleton(cls, *args, **kw):
    '''
    装饰器
    '''
    instances = {}
    @wraps(cls)
    def _singleton():
        print(instances)
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]
    return _singleton
    
@singleton
class MyClass():
    '''
    测试类
    '''
    a = 1
    def __init__(self, x=0):
        self.x = x
print(MyClass)
one = MyClass()
two = MyClass()

two.a = 3
print(one.a)
print(two.a)
