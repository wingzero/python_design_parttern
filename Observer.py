#!/usr/bin/python3.3
#coding=utf-8
'''
    观察者模式 Python3 实现
'''
from abc import ABCMeta,abstractmethod


class Subject():
    '''
    被观察者基类
    '''
    obsList = list()
    
    def AddObserver(self, o):
        self.obsList.append(o)
    
    def DelObserver(self, o):
        self.obsList.remove(o)

    def NotifyObservers(self):
        for o in self.obsList:
            o.Update()

class Observer(metaclass=ABCMeta):
    '''
    观察者基类 抽象类
    '''
    @abstractmethod
    def Update(self):
        pass
        
class ConcreteSubject(Subject):
    '''
    被观察者 实现类
    '''
    def Speak(self):
        self.NotifyObservers()
        
class ConcreteObserver(Observer):
    '''
    观察者 实现类
    '''
    def Update(self):
        print(self.__class__.__name__ + "update")
        
if __name__ == '__main__':
    subject = ConcreteSubject()
    obs = ConcreteObserver()
    subject.AddObserver(obs)
    subject.Speak()