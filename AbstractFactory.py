#! /usr/bin/env python
#coding=utf-8
'''
抽象工厂模式 Python3 实现
'''

from abc import ABCMeta,abstractmethod


class AbstractProductA(metaclass=ABCMeta):
    '''
    抽象产品A
    '''

    def shareMethod(self):
        print('产品A共通方法')

    @abstractmethod
    def doSomething(self):
        pass


class ProductA1(AbstractProductA):
    '''
    产品A1
    '''

    def doSomething(self):
        print('产品A1的实现方法')


class ProductA2(AbstractProductA):
    '''
    产品A2
    '''

    def doSomething(self):
        print('产品A2的实现方法')


class AbstractProductB(metaclass=ABCMeta):
    '''
    抽象产品B
    '''

    def shareMethod(self):
        print('产品B共通方法')

    @abstractmethod
    def doSomething(self):
        pass


class ProductB1(AbstractProductB):
    '''
    产品B1
    '''

    def doSomething(self):
        print('产品B1的实现方法')

class ProductB2(AbstractProductB):
    '''
    产品B2
    '''

    def doSomething(self):
        print('产品B2的实现方法')


class AbstractCreator(metaclass=ABCMeta):
    '''
    抽象工厂类
    '''

    @abstractmethod
    def createProductA(self):
        '''
        创建产品A家族
        '''
        pass

    @abstractmethod
    def createProductB(self):
        '''
        创建产品B家族
        '''
        pass


class Creator1(AbstractCreator):
    '''
    产品等级1的实现类
    '''

    def createProductA(self):
        return ProductA1()

    def createProductB(self):
        return ProductB1()


class Creator2(AbstractCreator):
    '''
    产品等级2的实现类
    '''

    def createProductA(self):
        return ProductA2()

    def createProductB(self):
        return ProductB2()


if __name__ == '__main__':
    creator1 = Creator1()
    creator2 = Creator2()
    a1 = creator1.createProductA()
    a2 = creator2.createProductA()
    b1 = creator1.createProductB()
    b2 = creator2.createProductB()
    a1.shareMethod()
    a1.doSomething()
    a2.doSomething()
    b1.doSomething()
    b2.doSomething()