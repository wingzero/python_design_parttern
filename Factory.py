#! /usr/bin/env python
#coding=utf-8
'''
工厂模式 Python 3 实现
'''

from abc import ABCMeta,abstractmethod


class Product(metaclass=ABCMeta):
    '''
    抽象产品
    '''

    def method1(self):
        print('method1')

    @abstractmethod
    def method2(self):
        pass


class ConcreteProduct1(Product):
    '''
    具体产品1
    '''

    def method2(self):
        print('ConcreteProduct1')


class ConcreteProduct2(Product):
    '''
    具体产品2
    '''

    def method2(self):
        print('ConcreteProduct2')


class Creator(metaclass=ABCMeta):
    '''
    抽象工厂
    '''

    @abstractmethod
    def createProduct(self,product):
        pass


class ConcreteCreator(Creator):
    '''
    具体工厂
    '''

    def createProduct(self, product):
        return product()


if __name__ == '__main__':
    a = ConcreteCreator()
    b = a.createProduct(ConcreteProduct1)
    c = a.createProduct(ConcreteProduct2)
    b.method1()
    b.method2()
    c.method2()
