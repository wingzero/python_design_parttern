#!/usr/bin/python3.3
#coding=utf-8
'''
模板方法模式的Python3 实现
'''

from abc import ABCMeta,abstractmethod


class AbstractClass(metaclass=ABCMeta):
    '''
    抽象模板类
    '''
    
    @abstractmethod
    def doSomething(self):
        '''
        基本方法
        '''
        
        pass
        
    @abstractmethod
    def doAnything(self):
        '''
        基本方法
        '''
        
        pass
        
    def templateMethod(self):
        self.doSomething()
        self.doAnything()
        
    
class ConcreteClass1(AbstractClass):
    '''
    具体模板类1
    '''
        
    def doSomething(self):
        print('模板类1变身')
        
    def doAnything(self):
        print('模板类1暴走')
        
     
        
class ConcreteClass2(AbstractClass):
    '''
    具体模板类1
    '''
        
    def doSomething(self):
        print('模板类2蓄力')
        
    def doAnything(self):
        print('模板类2必杀')
        
        
if __name__ == '__main__':
    class1 = ConcreteClass1()
    class2 = ConcreteClass2()
    class1.templateMethod()
    class2.templateMethod()