#!/usr/bin/python3.3
#coding=utf-8
'''
简单工厂模式 Python3 实现
'''

from abc import ABCMeta,abstractmethod


class Product(metaclass=ABCMeta):
    '''
    抽象产品
    '''
    
    def method1(self):
        print('method1')
    
    @abstractmethod
    def method2(self):
        pass


class ConcreteProduct1(Product):
    '''
    具体产品1
    '''
    
    def method2(self):
        print('ConcreteProduct1')


class ConcreteProduct2(Product):
    '''
    具体产品2
    '''
    
    def method2(self):
        print('ConcreteProduct2')
        
class SimpleFactory():
    
    @staticmethod
    def createProduct(product):
        return product()
        
        
if __name__ == '__main__':
    b = SimpleFactory.createProduct(ConcreteProduct1)
    c = SimpleFactory.createProduct(ConcreteProduct2)
    b.method1()
    b.method2()
    c.method2()